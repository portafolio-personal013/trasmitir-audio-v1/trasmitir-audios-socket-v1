const express = require('express');
const http = require('http');
const { Server } = require('socket.io');
const cors = require('cors');

const app = express();
const serverW = http.createServer(app);
const io = new Server(serverW, {
  cors: {
    origin: 'http://localhost:3000',
    methods: ['GET', 'POST'],
  },
});

app.use(cors({ origin: '*' }));

const PORT = 3001;
serverW.listen(PORT, () => {
  console.log(`Servidor en ejecución en el puerto ${PORT}`);
});

io.on('connection', (socket) => {
  console.log('Cliente conectado:', socket.id);

  socket.on('audio', (data) => {
    socket.broadcast.emit('audio', data);
  });

  socket.on('disconnect', () => {
    console.log('Cliente desconectado:', socket.id);
  });
});
