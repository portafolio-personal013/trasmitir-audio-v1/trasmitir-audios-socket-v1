Descripción:

Este Backend se encarga de emitir audio con la librería
socket.io a los clientes del Front que se conecten al canal "audio".


Versiones de librerías:

{
    "cors": "^2.8.5",
    "express": "^4.18.2",
    "nodemon": "^2.0.22",
    "socket.io": "^4.6.2"
}

Instalación y forma de correr el proyecto en Windows:

1) Abrir la consola de su sistema operativo o
terminal del editor de código.

2) Escribir y ejecutar npm i.

3) Escribir y ejecurar npm run start.


